FROM node:alpine as builder

WORKDIR /app

COPY . .
COPY package.json .
RUN npm install --include=dev && npm install -g nodemon && rm -rf node_module && npm cache clean --force

FROM node:alpine

WORKDIR /app

COPY --from=builder /app .

CMD ["npm","run","dev"]

#!/bin/bash

### Install docker ###
sudo apt install -y\
  ca-certificates \
  curl \
  gnupg \
  lsb-release
sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
echo \
"deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
$(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
sudo apt-get install -y docker-ce docker-ce-cli containerd.io docker-compose-plugin

### Install docker-compose ###
curl -SL https://github.com/docker/compose/releases/download/v2.14.0/docker-compose-linux-x86_64 -o /usr/local/bin/docker-compose
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
sudo usermod -aG docker $USER
newgrp docker
sudo usermod -aG docker docker-compose
#newgrp docker-compose

### Copy todo-list-app from git ###
git clone https://gitlab.deusops.com/deuslearn/classbook/todo-list-app.git

### Add docker-compose.yml file ###
touch /home/vagrant/todo-list-app/docker-compose.yml
echo "
version: '3'
services:
  app:
    image: node:18-alpine
    command: sh -c 'yarn install && yarn run dev'
    ports:
      - :3000:3000
    working_dir: /app
    volumes:
      - ./:/app
    environment:
      MYSQL_HOST: mysql
      MYSQL_USER: root
      MYSQL_PASSWORD: secret
      MYSQL_DB: todos
    labels:
      - 'traefik.enable=true'
      - 'traefik.http.routers.registry.rule=Host(`dev.example.com`)'
      - 'traefik.http.routers.traefik.service=api@internal'

  mysql:
    image: mysql:8.0
    volumes:
      - todo-mysql-data:/var/lib/mysql
    environment:
      MYSQL_ROOT_PASSWORD: secret
      MYSQL_DATABASE: todos

  traefik:
    image: 'traefik:v2.4'
    container_name: 'traefik'
    command:
      - '--api.insecure=true'
      - '--providers.docker=true'
      - '--providers.docker.exposedbydefault=false'
    ports:
      - 80:80
      - 8080:8080
    volumes:
      - '/var/run/docker.sock:/var/run/docker.sock:ro'
    labels:
      - 'traefik.enable=true'
      - 'traefik.http.routers.traefik.rule=Host(`traefik.com`)'
      - 'traefik.http.routers.traefik.entrypoints=websecure'
      - 'traefik.http.routers.traefik.tls.certresolver=myresolver'

networks:
  todo-list-app_default:
    external: true

volumes:
  todo-mysql-data:
" | tee -a /home/vagrant/todo-list-app/docker-compose.yml
cd /home/vagrant/todo-list-app/

### Build docker-compose.yml file ###
docker compose up -d

### Run it with name dev.examle.com ###
echo "127.0.0.1 dev.example.com" | sudo tee -a /etc/hosts

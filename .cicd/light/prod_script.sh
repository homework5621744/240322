### Install docker ###
sudo apt install -y\
  ca-certificates \
  curl \
  gnupg \
  lsb-release
sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
echo \
"deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
$(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
sudo apt-get install -y docker-ce docker-ce-cli containerd.io docker-compose-plugin

### Install docker-compose ###
curl -SL https://github.com/docker/compose/releases/download/v2.14.0/docker-compose-linux-x86_64 -o /usr/local/bin/docker-compose
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
sudo usermod -aG docker $USER
newgrp docker

### Copy todo-list-app from git ###
git clone https://gitlab.deusops.com/deuslearn/classbook/todo-list-app.git

### Add Dockerfile file ###
touch /home/vagrant/todo-list-app/Dockerfile
echo "
FROM node:alpine as builder

WORKDIR /app

COPY . .
COPY package.json .
RUN npm install --include=dev && npm install -g nodemon

FROM node:alpine

WORKDIR /app

COPY --from=builder /app .

ENV DB_HOST=192.168.56.5
ENV DB_PORT=3306
ENV DB_USER=root
ENV DB_PASS=secret

CMD ['npm','run','dev']
" | tee -a /home/vagrant/todo-list-app/Dockerfile

cd /home/vagrant/todo-list-app/

### Build Dockerfile ###
docker build -t todo-list .

### Run docker image todo-list ###
docker run -d --name todo-list -p 3000:3000 todo-list:latest

### Install mysql ###
sudo apt install -y mysql-server

### Configure mysql ###
sudo mysql_ssl_rsa_setup --uid=mysql
sudo systemctl restart mysql
sudo ufw allow mysql
touch /tmp/mysqlfile
sed 's/127.0.0.1/#127.0.0.1/' /etc/mysql/mysql.conf.d/mysqld.cnf > /tmp/mysqld.cnf
echo "CREATE DATABASE todos;
CREATE USER 'root'@'%' IDENTIFIED BY 'secret';
GRANT ALL PRIVILEGES ON todos.* TO 'root'@'%';
FLUSH PRIVILEGES;
exit" >> /tmp/mysqlfile
sudo mysql < /tmp/mysqlfile

### Install nginx and config proxy###
sudo apt install -y nginx
sudo ufw allow 'Nginx HTTP'
echo "
#user  nobody;
worker_processes  1;

events {
    worker_connections  1024;
}

http {
    include       mime.types;
    default_type  application/octet-stream;
    send_timeout 1800;
    sendfile        on;
    keepalive_timeout  6500;

    server {
        listen       80;
        server_name  example.com;

        location / {
          proxy_pass          http://localhost:3000;
          proxy_read_timeout 1800;
          proxy_connect_timeout 1800;
        }
    }
}" | sudo tee /etc/nginx/nginx.conf
sudo nginx -t
sudo systemctl restart nginx

### Run it with name examle.com ###
echo "127.0.0.1 example.com" | sudo tee -a /etc/hosts
